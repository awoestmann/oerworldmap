FROM node:20.10.0-bullseye
WORKDIR /srv/oerworldmap-ui

COPY docker/entrypoint-ui.sh /srv

ENTRYPOINT [ "/srv/entrypoint-ui.sh" ]
